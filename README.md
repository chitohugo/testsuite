## Running locally

### Pre-requisites:
- python 3.5 or higher
- virtualenv or venv

### Steps
1. Creation of virtual environments `virtualenv --python=python3 venv` or venv `python -m venv .venv`
2. Install requirements `pip install -r requirements.txt`