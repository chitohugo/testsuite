from appium import webdriver


class Driver(object):

    def __init__(self):
        desired_caps = {
            'platformName': 'Android',
            'platformVersion': '7.1',
            'automationName': 'UiAutomator2',
            'deviceName': 'cv109',
            'udid': 'LMX210c79281cb',
            'appPackage': 'co.coderio.arrivent',
            'appActivity': 'co.coderio.arrivent.MainActivity'
        }

        self.instance = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)
