import xlrd


class ReadExcel(object):
    @staticmethod
    def read_sheet(sheet_name=None, test_name=None):
        try:
            workbook = xlrd.open_workbook("../files/testData.xlsx")
            worksheet = workbook.sheet_by_name(sheet_name)
            dict_values = {}
            first_row = []
            for col in range(worksheet.ncols):
                first_row.append(worksheet.cell_value(0, col))
            for row in range(1, worksheet.nrows):
                if worksheet.cell(row, 0).value == test_name:
                    for col in range(worksheet.ncols):
                        dict_values[first_row[col]] = worksheet.cell_value(row, col)
            return dict_values
        except FileNotFoundError:
            return "File test not found"
