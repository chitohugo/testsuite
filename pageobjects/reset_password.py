import time


class ResetPassword(object):
    reset_pass_btn = None
    email_input = None
    token_input = None
    done_btn = None

    def __init__(self, driver):
        self.driver = driver

    def view_reset_password(self):
        self.reset_pass_btn = self.driver.instance.find_element_by_xpath(
            "//android.widget.TextView[@text='Olvidó su contraseña?']")

    def reset_password(self, email, token):
        self.view_reset_password()
        time.sleep(2)
        print("here")
        self.reset_pass_btn.click()
        self.email_input = self.driver.instance.find_element_by_xpath(
            "//android.widget.EditText[@text='Email']")
        self.email_input.send_keys(email)
        # self.token_input = self.driver.instance.find_element_by_xpath(
        #     "//android.widget.EditText[@text='Token']")
        # self.token_input.send_keys(token)
        # self.done_btn = self.driver.instance.find_element_by_xpath(
        #     "//android.view.ViewGroup[@index=5]")
