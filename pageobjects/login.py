class Login(object):
    email_input = None
    next_btn = None
    password_input = None
    done_btn = None

    def __init__(self, driver):
        self.driver = driver

    def view_email(self):
        self.email_input = self.driver.instance.find_element_by_xpath(
            "//android.widget.EditText[@text='Email']")
        self.next_btn = self.driver.instance.find_element_by_xpath(
            "//android.view.ViewGroup[@index=4]")

    def view_password(self):
        self.password_input = self.driver.instance.find_element_by_xpath(
            "//android.widget.EditText[@text='Contraseña']")
        self.done_btn = self.driver.instance.find_element_by_xpath(
            "//android.view.ViewGroup[@index=4]")

    def login(self, email, password):
        self.view_email()
        self.email_input.send_keys(email)
        self.next_btn.click()
        self.view_password()
        self.password_input.send_keys(password)
        self.done_btn.click()
