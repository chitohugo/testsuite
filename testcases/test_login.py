import time
import unittest
from webdriver.driver import Driver
from pageobjects.login import Login
from pageobjects.reset_password import ResetPassword
from common.read_excel import ReadExcel


class LoginTestCases(unittest.TestCase):
    def setUp(self):
        self.driver = Driver()
        self.driver.instance.implicitly_wait(100)

    def test_login_success(self):
        log_in = Login(self.driver)
        read_excel = ReadExcel()
        worksheet = "Login"
        test = LoginTestCases.test_login_success.__name__
        dict_values = read_excel.read_sheet(worksheet, test)
        log_in.login(dict_values['email'], dict_values['pass'])

    # def test_reset_password(self):
    #     t = ResetPassword(self.driver)
    #     t.reset_password("hugo.gonzalez@coderio.co", 123456)

    def tearDown(self):
        time.sleep(5)
        self.driver.instance.quit()


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(LoginTestCases)
    unittest.TextTestRunner(verbosity=2).run(suite)
